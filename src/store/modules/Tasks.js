import * as mutationTypes from './mutation-types';
import * as actionTypes from './action-types';


const state = {
  tasks: [],
  searchText: '',
};

const getters = {
  getTaskById: state => id => state.tasks.find(task => task.TaskID === id),
  getSearchedTasks: state => state.tasks.filter(task => new RegExp(state.searchText, 'i').test(task.TaskTag)),
};

const mutations = {
  [mutationTypes.ADD_TASK](state, task) {
    state.tasks.push({ TaskTag: task, TaskID: Symbol(task) });
  },

  [mutationTypes.SET_SEARCH](state, searchText) {
    state.searchText = searchText;
  },

  [mutationTypes.DELETE_TASK](state, task) {
    if (task) {
      state.tasks = state.tasks.filter(item => item !== task);
    }
  },

  [mutationTypes.EDIT_TASK](state, payload) {
    state.tasks.splice(payload.index, 1, payload.task);
  },
};

const actions = {
  [actionTypes.ADD_TASK]({ commit }, payload) {
    commit(mutationTypes.ADD_TASK, payload);
  },

  [actionTypes.EDIT_TASK]({ state, commit, getters }, payload) {
    const taskIndex = state.tasks.indexOf(getters.getTaskById(payload.id));

    commit(mutationTypes.EDIT_TASK, { task: payload, index: taskIndex });
  },

  [actionTypes.DELETE_TASK]({ commit, getters }, payload) {
    commit(mutationTypes.DELETE_TASK, getters.getTaskById(payload));
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
