import * as mutationTypes from './mutation-types';

const state = {
  rowsPerPage: 5,
  page: 1,
  pagination: true,
  popupId: '',
  message: '',
};

const mutations = {
  [mutationTypes.SET_MESSAGE](state, message) {
    state.message = message;
  },

  [mutationTypes.SET_POPUP_ID](state, popupId) {
    state.popupId = popupId;
  },

  [mutationTypes.SET_PAGE](state, page) {
    state.page = page;
  },

  [mutationTypes.SET_ROWS_PER_PAGE](state, rowsPerPage) {
    state.rowsPerPage = rowsPerPage;
  },

  [mutationTypes.SET_PAGINATION_VISIBILITY](state, pagination) {
    state.pagination = pagination;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
};
