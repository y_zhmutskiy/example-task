import Vue from 'vue';
import App from './App.vue';
import store from './store';

// Common components
import customInput from './components/customInput.vue';

Vue.component('custom-input', customInput);
Vue.config.productionTip = false;

// Directives
Vue.directive('popup', {
  bind(el, binding) {
    el.addEventListener('click', () => {
      try {
        store.commit('Common/SET_POPUP_ID', binding.value);
      } catch (err) {
        console.error("Value must be 'popupID'", err);
      }
    });
  },
});

new Vue({
  render: h => h(App),
  store,
}).$mount('#app');
